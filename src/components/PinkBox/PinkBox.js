import React from "react";

class PinkBox extends React.Component {
  componentDidUpdate() {
    console.log(this.props);
  }

  render() {
    return (
      <div
        style={{
          backgroundColor: "pink",
          position: "absolute",
          width: 100,
          height: 100,
          left: this.props.x,
          top: this.props.y
        }}
      >
        &nbsp;
      </div>
    );
  }
}

export default PinkBox;
