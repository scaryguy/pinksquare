import React, { Component } from "react";
import styles from "./App.module.css";
import PinkBox from "../../components/PinkBox/PinkBox";

class App extends Component {
  state = {
    x: 125,
    y: 125
  };

  handleClick = e => {
    console.log(e.target.id);
    switch (e.target.id) {
      case "top":
        if (this.state.y === 0) return;
        this.setState(prevState => {
          return {
            ...prevState,
            y: prevState.y - 25
          };
        });
        break;
      case "bottom":
        if (this.state.y === 250) return;
        this.setState(prevState => {
          return {
            ...prevState,
            y: prevState.y + 25
          };
        });
        break;

      case "left":
        if (this.state.x === 0) return;
        this.setState(prevState => {
          return {
            ...prevState,
            x: prevState.x - 25
          };
        });
        break;
      case "right":
        if (this.state.x === 250) return;
        this.setState(prevState => {
          return {
            ...prevState,
            x: prevState.x + 25
          };
        });
        break;
      default:
        return;
    }
  };

  render() {
    return (
      <main className={styles.App}>
        <section>
          <div className={styles.top}>
            <button id="top" onClick={e => this.handleClick(e)} />
          </div>
          <div className={styles.left}>
            <button id="left" onClick={e => this.handleClick(e)} />
          </div>
          <div className={styles.stage}>
            <PinkBox x={this.state.x} y={this.state.y} />
          </div>
          <div className={styles.right}>
            <button id="right" onClick={e => this.handleClick(e)} />
          </div>
          <div className={styles.bottom}>
            <button id="bottom" onClick={e => this.handleClick(e)} />
          </div>
        </section>
      </main>
    );
  }
}

export default App;
